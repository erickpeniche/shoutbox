module.exports = function(db) {
	var Entry = require('../lib/entry');
	var express = require('express');
	var router = express.Router();
	var bodyParser = require('body-parser');

	// create application/x-www-form-urlencoded parser
	var urlencodedParser = bodyParser.urlencoded({ extended: false });

	/* GET home page. */
	router.get('/', function(req, res) {
		Entry.getEntries(db, function(err, data) {
			var entries = null;
			if (err) {
				console.error(err);
			} else {
				if(data.length) {
					entries = data;
				}
			}
			res.render('index', { title: 'Entries', entries: entries });
		});
	});

	/* GET post form page. */
	router.get('/post', function(req, res) {
		res.render('post', { title: 'Entries', message: "" });
	});

	/* POST entry form page. */
	router.post('/post', urlencodedParser, function(req, res) {
		var entry = new Entry({
			"email": req.body.email,
			"title": req.body.title,
			"body": req.body.message
		});

		var message = "";

		entry.save(db, function(err) {
			if (err) {
				message = "The entry could not be saved.";
			} else {
				message = "Entry added successfully";
			}
		});

		console.log(message);

		res.render('post', { title: 'Entries', message: message });
	});

	return router;
}