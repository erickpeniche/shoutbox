module.exports = function(db) {
	var express = require('express');
	var router = express.Router();
	var Entry = require('../lib/entry');

	/* GET entries. */
	router.get('/entries', function(req, res) {
		var page = req.page;
		Entry.getEntries(db, function(err, data) {
			var entries = null;
			if (err) {
				console.error(err);
			} else {
				if(data.length) {
					entries = data;
				}
			}

			res.format({
				json: function(){
					res.send(entries);
				},

				xml: function(){
					res.render('xml', { entries: entries });
				}
			});
		});
	});

	return router;
}