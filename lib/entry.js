function Entry(obj) {
	for (var key in obj) {
		this[key] = obj[key];
	}
}

Entry.prototype.save = function(db, fn){
	//var entryJSON = JSON.stringify(this);

	db.query(
		"INSERT INTO entries (email, body, title) VALUES (?, ?, ?)",
		[this.email, this.body, this.title],
		function(err,results){
			if (err) {
				console.error(err);
				return fn(err);
			} else {
				console.dir(results);
				return fn();
			}
		}
	);
};

Entry.getEntries = function(db, fn){
	db.query(
		"SELECT * FROM entries",
		function(err,results){
			if (err) {
				console.error(err);
				fn(err);
			} else {
				fn(null, results);
			}
		}
	);
}

module.exports = Entry;